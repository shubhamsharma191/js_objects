const defaults = (obj = {}, defaultProps) =>{

    if (!typeof(obj) === Object){
         return {};
     }

     let newObject = obj;
     for(let key in defaultProps){
         if(newObject[key] === undefined){
             newObject[key] = defaultProps[key];
         }
     }
     return newObject; 
 
    }

module.exports = defaults;
