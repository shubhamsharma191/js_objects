const values = (obj) => {

    if (!typeof(obj) === Object){
        return {};
    }
    let arr = [];
    //console.log(obj)
    for(let key in obj){
        let value = obj[key]

        if(typeof(value) === 'function'){
            continue;
        }
        arr.push(value)
    }
    return arr

}

module.exports = values;