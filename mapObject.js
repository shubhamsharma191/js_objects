const mapObject = (obj, cb) => {

    if (!typeof(obj) === Object){
       // console.log(obj)
        return {};
    }

    let newObject = {}
    for(let key in obj){
        //console.log(newObj)
        newObject[key] = cb(obj[key], key) 
    }
    //console.log(newObj)
    return newObject;
}

module.exports = mapObject