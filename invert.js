const invert = (obj) =>{

    if (!typeof(obj) === Object){
         return {};
     }

    let newObject = {};
    for(let key in obj){

        let val = obj[key]

        if(typeof(val) === Object){

            val = JSON.stringify(val); // string serializable
        }

        newObject[val] = key;
    }
    return newObject;

}

module.exports = invert;